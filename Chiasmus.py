import spacy
from spacy.tokens import Token
from itertools import groupby 
class Chiasmus:
    wA = None
    wB = None
    wC = None
    wD = None
    stopWords = ["-","the"]

    def __init__(self, arg1, arg2, arg3, arg4):
        self.wA=arg1
        self.wB=arg2
        self.wC=arg3
        self.wD=arg4
    def is_stop(self,wX):
        """Returns True lemma of wX is a stop word
        >>> is_stop(the)
        True
        """
        if wX.lemma_ in self.stopWords:
            return True
        else:
            return False
    def get_sentences(self):
        """Given a chiasmus the list of sentences in which the words are """
        sentences = [self.wA.sent,self.wB.sent,self.wC.sent,self.wD.sent]
        sentences = [i[0] for i in groupby(sentences)]
        return sentences
    def score(self):
        if self.is_stop(self.wA):
            return 0
        else:
            return 1
