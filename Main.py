#TODO: Make a chiasmus display function
#TODO: Make a chiasmus ranking function
#TODO: save a miniMain
#TODO: function that returns sentence highlighted
#Some elegant code here: https://github.com/kennethreitz/requests/blob/master/requests/api.py
#setters and getters: https://www.geeksforgeeks.org/getter-and-setter-in-python/
import spacy
from Chiasmus import Chiasmus
from spacy.tokens import Token
f = open('miniExample.txt', 'r')
text= f.read()
nlp = spacy.load('en_core_web_sm')
doc = nlp(text)

Token.set_extension("set_wX", default=False)
print(doc[3]._.set_wX)
def find_sentences_with_wX(iA,iB,iC,iD):
    new_doc = nlp(text)# We initiate a virgin new_doc.
    new_doc[iA]._.set_wX = True
    new_doc[iB]._.set_wX = True
    new_doc[iC]._.set_wX = True
    new_doc[iD]._.set_wX = True
    for token in new_doc[iA].sent:
        if token._.set_wX:
            print("{"+token.text+"}")
        else:
            print(token)
iA=0
chiasmusList = []
for wA in doc:
    iD=iA+1
    for wD in doc[iA+1:iA+30]:#this loop check 30 words after word A in order to catch an identical pair A A' (or wA wD pair)
        if wA.lemma_==wD.lemma_:				
            iB=iA+1#iB is the position of wB
            for wB in doc[iA+1:iD-1]:#only up to D-2
                iC=iB+1
                for wC in doc[iB+1:iD]:#Do not forget that the end limit is NOT inclusive so NO iD-1
                    if wB.lemma_!=wC.lemma_:
                        iC=iC+1    
                    else:
                        chiasmusList+=[doc[iA:iD]]
                        #find_sentences_with_wX(iA,iB,iC,iD)
                        candidate = Chiasmus(wA,wB,wC,wD)
                        c = candidate
                        print(c.score())
                        print(c.get_sentences())
                        iC=iC+1
                iB=iB+1
        iD=iD+1	
    iA=iA+1


#chiasmusList = list(dict.fromkeys(chiasmusList))
#print(chiasmusList)