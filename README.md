# Chiasmus Detection Library



## Prerequisites with Virtual env
If you use virtual env you can directly run these commands:
```bash
cd path/to/chiasmus-detector
python3 -m venv chiasmus_env
source chiasmus_env/bin/activate
pip install -r requirements.txt
bash run.sh
```

## Citation

If you use this implementation in your work, please cite:

```
@ARTICLE{dubremetz2018,
  
AUTHOR={Dubremetz, Marie and Nivre, Joakim},   
	 
TITLE={Rhetorical Figure Detection: Chiasmus, Epanaphora, Epiphora},      
	
JOURNAL={Frontiers in Digital Humanities},      
	
VOLUME={5},      

PAGES={10},     
	
YEAR={2018},      
	  
URL={https://www.frontiersin.org/article/10.3389/fdigh.2018.00010},       
	
DOI={10.3389/fdigh.2018.00010},      
	
ISSN={2297-2668},   
   
ABSTRACT={Rhetorical figures are valuable linguistic data for literary analysis. In this article, we target the detection of three rhetorical figures that belong to the family of repetitive figures: chiasmus (I go where I please, and I please where I go.), epanaphora also called anaphora (“Poor old European Commission! Poor old European Council.”) and epiphora (“This house is mine. This car is mine. You are mine.”). Detecting repetition of words is easy for a computer but detecting only the ones provoking a rhetorical effect is difficult because of many accidental and irrelevant repetitions. For all figures, we train a log-linear classifier on a corpus of political debates. The corpus is only very partially annotated, but we nevertheless obtain good results, with more than 50% precision for all figures. We then apply our models to totally different genres and perform a comparative analysis, by comparing corpora of fiction, science and quotes. Thanks to the automatic detection of rhetorical figures, we discover that chiasmus is more likely to appear in the scientific context whereas epanaphora and epiphora are more common in fiction.}
}
```
